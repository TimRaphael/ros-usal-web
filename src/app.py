#!/usr/bin/python

from flask import Flask
from flask import render_template
from flask import request

import rospy
from std_msgs.msg import String

app = Flask("usal_web")

@app.route('/')
def root():
	rospy.init_node('usal_direction', anonymous=True)
	return render_template("index.html")

@app.route('/direction', methods=['POST'])
def directionURL():
	direction = str(request.get_data())

	publisher = rospy.Publisher('usal_dir_cmd', String, queue_size=10)
	publisher.publish(direction)	

	print "Sent cmd: " + direction	

	return "True"


if __name__ == '__main__':
	# setup the ROS Node
	app.run(host="0.0.0.0", port=8000)
